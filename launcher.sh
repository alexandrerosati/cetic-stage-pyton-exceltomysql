#!/bin/bash
clear
echo -e '\x1B[32m===================================='
echo -e '=          \x1B[01;95mScript AccessI\x1B[0m\x1B[32m          ='
echo -e '=   \x1B[01;95mAuthor  :\x1B[0m Alexandre Rosati\x1B[32m     ='
echo -e '=   \x1B[01;95mCompany :\x1B[0m CETIC Gosselies\x1B[32m      ='
echo -e '=   \x1B[01;95mDate    :\x1B[0m 25/02/15 01:42 PM\x1B[32m    ='
echo -e '===================================='
echo -e '\x1B[01;95m1.\x1B[0m Clean data & config directory'


if [ -f 'config/batiment.yml' ]
then
  rm config/batiment.yml
  echo -e '\x1B[01;92mFILE :: config/batiment.yml removed \x1B[0m'
fi

if [ -f 'config/contact.yml' ]
then
  rm config/contact.yml
  echo -e '\x1B[01;92mFILE :: config/contact.yml removed \x1B[0m'
fi
if [ -f 'config/label.yml' ]
then
  rm config/label.yml
  echo -e '\x1B[01;92mFILE :: config/label.yml removed \x1B[0m'
fi

if [ -f 'data/batiment.csv' ]
then
  rm data/batiment.csv
  echo -e '\x1B[01;92mFILE :: data/batiment.csv removed \x1B[0m'
fi
if [ -f 'data/contact.csv' ]
then
  rm data/contact.csv
  echo -e '\x1B[01;92mFILE :: data/contact.csv removed \x1B[0m'
fi
if [ -f 'data/label.csv' ]
then
  rm data/label.csv
  echo -e '\x1B[01;92mFILE :: data/label.csv removed \x1B[0m'
fi

echo -e '\x1B[01;95m2.\x1B[0m Convert file from latin1 to utf8'
python manip/convertUTF8.py --source ./data/accessi.csv --cible ./data/data.csv
echo -e '\x1B[01;95m3.\x1B[0m Parsing data'
python manip/accessi.py --source ./data/data.csv
echo -e '\x1B[01;95m4.\x1B[0m Generate file with mETL'
metl-generate --resource data/batiment.csv --headerRow 0 --skipRows 1 --delimiter ";" csv config/batiment.yml
metl-generate --resource data/label.csv --headerRow 0 --skipRows 1 --delimiter ";" csv config/label.yml
metl-generate --resource data/contact.csv --headerRow 0 --skipRows 1 --delimiter ";" csv config/contact.yml
echo -e '\x1B[01;95m5.\x1B[0m Update sql target to config file'
python manip/config.py --source ./config/batiment.yml
python manip/config.py --source ./config/label.yml
python manip/config.py --source ./config/contact.yml
echo -e '\x1B[01;95m6.\x1B[0m Upload data to MySQL'
metl config/batiment.yml
metl config/contact.yml
metl config/label.yml
metl config/batiment.yml
metl config/contact.yml
metl config/label.yml
echo -e '\x1B[32m===================================='
echo -e '=          \x1B[01;95mEnd of script\x1B[0m\x1B[32m           ='
echo -e '= \x1B[01;95mcontact    : \x1B[0malex7170@gmail.com\x1B[32m  ='
echo -e '====================================\x1B[0m'
