1. Introduction
------

AccessI est un projet qui a pour but de transformer et de sauvegarder des données au format CSV vers MySQL

Le jeu de donnée provient de http://www.access-i.be/, le probléme étant que les données sont brut et mal organisé, nous diviserons ces data en 3 parties: batiment, contact & label.

2. Utilisation
------


### 2.1. Clonage du projet
```
git clone http://git.cetic.be/Alexandre/AccessI.git
```

### 2.2. Execution automatique via un script shell
```
time sh -c "/path/to/dir/AccessI/launcher.sh"
```
![alt text](http://i.imgur.com/VnrCpVc.png)
### 2.3. exécution manuelle

#### 2.3.1. Encodage utf8 du fichier source
```
python manip/convertUTF8.py --source /path/to/source.csv --cible /path/to/cible.csv
```

#### 2.3.2. Decomposition des données en 3 parties
```
python manip/accessi.py --source /path/to/source.csv
```

#### 2.3.3. Generation des fichiers de configuration mETL
```
metl-generate --resource /path/to/file.csv --headerRow 0 --skipRows 1 --delimiter ";" csv path/to/config/file.yml
```
Répéter 3 fois l'opération pour chaqu'un des fichiers se trouvant dans ```/path/to/AccessI/data/```

#### 2.3.4. Modification des fichiers de configuration et ajout de la target SQL
```
python manip/config.py --source /path/to/config/file.yml
```
Répéter 3 fois l'opération pour chaqu'un des fichiers se trouvant dans ```/path/to/config/```

#### 2.3.5. Envoie des données vers la base de donnée
```
metl /path/to/config/file.yml
```
Répéter 3 fois l'opération pour chaqu'un des fichiers se trouvant dans ```/path/to/config/```

**Remarque:** Il se peut que les données sur MySQL soit au format latin1, veuillez verifier votre encodage de la base de donnée et répété le script
