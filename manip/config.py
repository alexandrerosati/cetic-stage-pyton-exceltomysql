#!/usr/bin/env python
# -*- coding: utf-8 -*-

import yaml
import argparse

parser = argparse.ArgumentParser(description='Parser pour fichier de configuration mETL.Ajout de la target MySQL')

parser.add_argument('--source', action="store", dest="source", help='fichier source de configuration')


list_string = ["date_fiche", "date_releve", "operateur", "asbl", "date_fin_label", "date_derniere_label", "date_premiere_label", "site", "mail", "fax", "telephone", "adresse", "date_debut", "date_fin", "date_type", "province", "ville", "code_postal", "titre", "type", "status", "auditeur", "gestionnaire", "categorie", "lvl_chaise_roulante", "lvl_marchant_difficilement", "lvl_aveugle", "lvl_malvoyante", "lvl_sourde", "lvl_malentendante", "lvl_difficulte_comprehension", "prenom", "nom", "telephone", "fax", "gsm", "mail"]
list_text = ["remarques", "description", "text_plus_chaise_roulante", "text_plus_marchant_difficilement", "text_plus_aveugle", "text_plus_malvoyante", "text_plus_sourde", "text_plus_malentendante", "text_plus_difficulte_comprehension", "text_moins_chaise_roulante", "text_moins_marchant_difficilement", "text_moins_aveugle", "text_moins_malvoyante", "text_moins_sourde", "text_moins_malentendante", "text_moins_difficulte_comprehension", "adaptations_chaise_roulante", "adaptations_marchant_difficilement", "adaptations_aveugle", "adaptations_malvoyante", "adaptations_sourde", "adaptations_malentendante", "adaptations_difficulte_comprehension", "text_adaptations", "text_pour_tous"]
list_integer = ["fk_label", "fk_contact", "id_label", "id_contact", "id_batiment"]
file = parser.parse_args().source
def parserConfig():
    stream = open(file, 'r')
    file_parse = file.split('.yml')
    file_parse = file_parse[0].split('/')

    data = yaml.load(stream)
    '''
    changement des types de chaque fiels
    '''
    for  j in data["source"]["fields"]:
        if j["map"] in list_string:
            j["type"] = "String"
        elif j["map"] in list_text:
            j["type"] = "Text"
        elif j["map"] in list_integer:
            j["type"] = "Integer"
        else:
            j["map"] = String
    '''
    configuration Target
    '''
    del data["target"]["silence"]
    data["target"]["type"] = "Database"
    data["target"]["url"] = "postgresql://postgres:azerty@localhost:5432/accessi"
    data["target"]["createTable"] = True
    data["target"]["truncateTable"] = True
    data["target"]["table"] = "accessi_%s" % (file_parse[2])
    with open(file, 'w') as outfile:
        outfile.write(yaml.dump(data, default_flow_style=False))
if __name__ == "__main__":
    parserConfig()

