#!/usr/bin/env python
# -*- coding: utf-8 -*-

import csv
import re
import argparse

parser = argparse.ArgumentParser(description='Manipulation des données et organisation')

parser.add_argument('--source', action="store", dest="source", help='fichier source de configuration')

global url_source, out_batiment, out_contact, out_label
# == FILE IN/OUT ==
url_source = parser.parse_args().source
out_batiment = "./data/batiment.csv"
out_contact = "./data/contact.csv"
out_label = "./data/label.csv"
# == LIST OF ELEMENTS ==
batiment = []
contact = []
label = []

def checkContact(prenom, nom, telephone, fax, gsm, mail):
    trouve = -1
    last = -1
    retour = -1
    for i, item in enumerate(contact):
        last = i
        if item[0] == prenom and item[1] == nom and item[2] == telephone:
            trouve = i
    if trouve == -1:

        contact.append([last+1, nom, prenom, telephone, fax, gsm, mail])
        retour = last + 1
        contact.append([retour, prenom, nom, telephone, fax, gsm, mail])
    else:
        retour = trouve
    return retour

def separateText(label_id, text):
    try:
        data = text
        plus = ""
        moins = ""
        data_1 = re.compile("(p|P)oint(s|\\b) (f|F)ort(s|\\b)").split(data)
        for i in data_1:
            if i != "" and i != "P" and i != "s" and i != "f" :
                data_1_re = i
        data_2 = re.compile("(p|P)oint(s|\\b) (f|F)aible(s|\\b)").split(data_1_re)
        data_2_count = 0
        for i in data_2:
            data_2_count = data_2_count + 1
        if data_2_count > 1 :
            if data_2[0] != "":
                plus = data_2[0]                
            moins = data_2[5]
        else:
            plus = data_2[0]
    except:
        plus = moins = ""
        print '[ERROR] label_id="%d" value="%s"'%(label_id, text)
        print '[ERROR] Conseil : veuillez respecter le format Points forts & Points faible'
    return [plus, moins]    
        
def loadLabelisation(label_id, label_1, label_2, label_3, label_4, label_5, label_6, label_7,
                     text_1, text_2, text_3, text_4, text_5, text_6, text_7,
                     adapt_1, adapt_2, adapt_3, adapt_4, adapt_5, adapt_6, adapt_7,
                     text_adaptations, text_pour_tous):
    text_split_1 = separateText(label_id, text_1)
    text_split_2 = separateText(label_id, text_2)
    text_split_3 = separateText(label_id, text_3)
    text_split_4 = separateText(label_id, text_4)
    text_split_5 = separateText(label_id, text_5)
    text_split_6 = separateText(label_id, text_6)
    text_split_7 = separateText(label_id, text_7)
    label.append([label_id, label_1, label_2, label_3, label_4, label_5, label_6, label_7,
                 text_split_1[0], text_split_2[0], text_split_3[0], text_split_4[0], text_split_5[0], text_split_6[0], text_split_7[0],
                 text_split_1[1], text_split_2[1], text_split_3[1], text_split_4[1], text_split_5[1], text_split_6[1], text_split_7[1],
                 adapt_1, adapt_2, adapt_3, adapt_4, adapt_5, adapt_6, adapt_7,
                 text_adaptations, text_pour_tous])
                 
def loadFile():
    with open(url_source, "rb") as csvfile:
        data = csv.reader(csvfile, delimiter=';', quotechar="\"")
        label_id = 0
        for i, row in enumerate(data):
            if i > 0:
                contact_id = checkContact(row[42], row[43], row[44], row[45], row[46], row[47])
                loadLabelisation(label_id, row[19], row[20], row[21], row[22], row[23], row[24], row[25], #label_lvl
                             row[27], row[28], row[29], row[30], row[31], row[32], row[33], #text pour x label
                             row[34], row[35], row[36], row[37], row[38], row[39], row[40], #adaptations pour x label
                             row[41], row[26]) #text adaptations + text pour tous
                batiment.append([row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10], row[11], row[12], row[13], row[14], row[15], row[16], row[17], row[18], contact_id, label_id, row[48], row[49], row[50], row[51], row[52], row[53], row[54], row[55]])
                label_id = label_id + 1
        
        with open(out_batiment, 'wb') as csvbatiment:
            spamwriter = csv.writer(csvbatiment, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            spamwriter.writerow(["id_batiment", "titre", "type", "status", "auditeur", "gestionnaire", "categorie", "description", "date_debut", "date_fin", "date_type", "province", "ville", "code_postal", "adresse", "telephone", "fax", "mail", "site", "fk_label", "fk_contact", "date_premiere_label", "date_derniere_label", "date_fin_label", "asbl", "operateur", "date_releve", "remarques", "date_fiche"])
            for i in batiment:
                spamwriter.writerow(i)
        with open(out_contact, 'wb') as csvcontact:
            spamwriter = csv.writer(csvcontact, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            spamwriter.writerow(["id_contact","nom", "prenom", "telephone", "fax", "gsm", "mail"])
            for i in contact:
                spamwriter.writerow(i)
        with open(out_label, 'wb') as csvlabel:
            spamwriter = csv.writer(csvlabel, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            spamwriter.writerow(["id_label", "lvl_chaise_roulante", "lvl_marchant_difficilement", "lvl_aveugle", "lvl_malvoyante", "lvl_sourde", "lvl_malentendante", "lvl_difficulte_comprehension", "text_plus_chaise_roulante", "text_plus_marchant_difficilement", "text_plus_aveugle", "text_plus_malvoyante", "text_plus_sourde", "text_plus_malentendante", "text_plus_difficulte_comprehension", "text_moins_chaise_roulante", "text_moins_marchant_difficilement", "text_moins_aveugle", "text_moins_malvoyante", "text_moins_sourde", "text_moins_malentendante", "text_moins_difficulte_comprehension", "adaptations_chaise_roulante", "adaptations_marchant_difficilement", "adaptations_aveugle", "adaptations_malvoyante", "adaptations_sourde", "adaptations_malentendante", "adaptations_difficulte_comprehension", "text_adaptations", "text_pour_tous"])
            for i in label:
                spamwriter.writerow(i)
if __name__ == "__main__":
    loadFile()


'''
============================
==  Structure de batiment ==
============================ 
["id_batiment", "titre", "type", "status", "auditeur", "gestionnaire", "categorie", "description", "date_debut", "date_fin", "date_type", "province",
"ville", "code_postal", "adresse", "telephone", "fax", "mail", "site", "fk_label", "fk_contact", "date_premiere_label", "date_derniere_label", "date_fin_label",
"asbl", "operateur", "date_releve", "remarques" ,"date_fiche"]
0   : id_batiment
1   : titre
2   : type
3   : status
4   : auditeur
5   : gestionnaire
6   : categorie
7   : description
8   : date_debut
9   : date_fin
10  : date_type
11  : province
12  : ville
13  : code_postal
14  : adresse
15  : telephone
16  : fax
17  : mail
18  : site
19  : fk_label
20  : fk_contact
21  : date_premiere_label
22  : date_derniere_label
23  : date_fin_label
24  : asbl
25  : operateur
26  : date_releve
27  : remarques
28  : date_fiche
==========================
== structure de contact ==
==========================
["id_contact", "prenom", "nom", "telephone", "fax", "gsm", "mail"]
0   : id_contact
1   : prenom
2   : nom
3   : telephone
4   : fax
5   : gsm  
6   : mail
========================
== structure de label ==
========================
["id_label", "lvl_chaise_roulante", "lvl_marchant_difficilement", "lvl_aveugle", "lvl_malvoyante", "lvl_sourde", "lvl_malentendante", "lvl_difficulte_comprehension",
"text_plus_chaise_roulante", "text_plus_marchant_difficilement", "text_plus_aveugle", "text_plus_malvoyante", "text_plus_sourde", "text_plus_malentendante", "text_plus_difficulte_comprehension",
"text_moins_chaise_roulante", "text_moins_marchant_difficilement", "text_moins_aveugle", "text_moins_malvoyante", "text_moins_sourde", "text_moins_malentendante", "text_moins_difficulte_comprehension",
"adaptations_chaise_roulante", "adaptations_marchant_difficilement", "adaptations_aveugle", "adaptations_malvoyante", "adaptations_sourde", "adaptations_malentendante", "adaptations_difficulte_comprehension",
"text_adaptations"]
0   : id_label
1   : lvl_chaise_roulante
2   : lvl_marchant_difficilement
3   : lvl_aveugle
4   : lvl_malvoyante
5   : lvl_sourde
6   : lvl_malentendante
7   : lvl_difficulte_comprehension
8   : text_plus_chaise_roulante 
9   : text_plus_marchant_difficilement
10  : text_plus_aveugle
11  : text_plus_malvoyante
12  : text_plus_sourde
13  : text_plus_malentendante
14  : text_plus_difficulte_comprehension
15  : text_moins_chaise_roulante
16  : text_moins_marchant_difficilement
17  : text_moins_aveugle
18  : text_moins_malvoyante
19  : text_moins_sourde
20  : text_moins_malentendante
21  : text_moins_difficulte_comprehension
22  : adaptations_chaise_roulante
23  : adaptations_marchant_difficilement
24  : adaptations_aveugle
25  : adaptations_malvoyante
26  : adaptations_sourde
27  : adaptations_malentendante
28  : adaptations_difficulte_comprehension
29  : text_adaptations
30   : text_pour_tous
==========================
== structure de accessi ==
==========================
0   : Id 
1   : titre
2   : Type
3   : Status
4   : Auditeur
5   : Gestionnaire
6   : Catégorie
7   : Texte
8   : Date de début
9   : Date de fin
10  : Type de date
11  : Province
12  : Ville
13  : Code Postal
14  : Adresse
15  : Téléphone
16  : Fax
17  : E-mail
18  : Site
19  : Label personne en chaise roulante
20  : Label personne marchant difficilement
21  : Label personne aveugle
22  : Label personne malvoyante
23  : Label personne sourde
24  : Label personne malentendante
25  : Label personne avec des difficulté de compréhension
26  : Texte pour tous
27  : Texte personne en chaise roulante
28  : Texte personne marchant difficilement
29  : Texte personne aveugle
30  : Texte personne malvoyante
31  : Texte personne sourde
32  : Texte personne malentendante
33  : Texte personne avec des difficulté de compréhension
34  : Adaptations personne en chaise roulante
35  : Adaptations personne marchant difficilement
36  : Adaptations personne aveugle
37  : Adaptations personne malvoyante
38  : Adaptations personne sourde
39  : Adaptations personne malentendante
40  : Adaptations personne avec des difficulté
41  : Texte adaptations
42  : Prénom du contact
43  : Nom du contact
44  : Téléphone du contact
45  : Fax du contact
46  : GSM du contact
47  : Email du contact
48  : Date 1ere labelisation
49  : Date du dernier renouvellement
50  : Date de fin de labelisation
51  : ASBL
52  : Personne ayant effectué le relevé
53  : Date du relevé
54  : Remarques
55  : Date de création de la fiche
'''
