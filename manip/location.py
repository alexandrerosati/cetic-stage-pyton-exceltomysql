#-*- coding: utf-8 -*-
import re
import psycopg2
from geopy.geocoders import Nominatim
from geopy.exc import GeocoderTimedOut

#================================#
#	   info sql
#================================#
database	= "accessi"
host		= "localhost"
user		= "postgres"
password	= "azerty"
#================================#



conVar = "dbname='%s' user='%s' host='%s' password='%s'" % (database, user, host, password)

pos = []
id_batiment = []
sql = {}
def getLocation(address):
	geolocator = Nominatim()
	try:
		location = geolocator.geocode(address, timeout=30)
		pos.append([location.latitude, location.longitude])
	except GeocoderTimedOut as e:
		print("Error: geocode failed on input %s with message %s"%(address, e))
	
def loadData():
	conn = psycopg2.connect(conVar)
	cur = conn.cursor()
	cur.execute("""select id_batiment, adresse, ville, code_postal from accessi_batiment """)
	rows = cur.fetchall()
	for row in rows:
		try:
			id_batiment.append(row[0])
			ads = "%s %s %s belgique" % (row[1], row[3], row[2])
			getLocation(ads)
		except:
			'''
			print "======= not found ======="
			print "1. get location from city"
			print "2. enter manualy the location"
			choix = raw_input("choose :: ")
			if choix == "1":
				ads = "%s %s belgique" % (row[3], row[2])
				getLocation(ads)
			elif choix == "2":
				print "> get location from city"
				print "> address : %s" % ads
				print "> please get the position on google map"
				latitude = raw_input(">>> latitude : ")
				longitude = raw_input(">>> longitude : ")
				print "> output : %s, %s" % (latitude, longitude)
				pos.append([latitude, longitude])
			else :
				ads = "%s %s belgique" % (row[3], row[2])
				getLocation(ads)
			'''
			ads = "%s %s belgique" % (row[3], row[2])
			getLocation(ads)
def uploadData():
	conn = psycopg2.connect(conVar)
	cur = conn.cursor()
	for i, j in enumerate(id_batiment):
		cur.execute("""INSERT INTO accessi_location(id_batiment, latitude, longitude) VALUES (%s, %s, %s)""" % (j, pos[i][0], pos[i][1]))
		conn.commit()

if __name__ == "__main__":
	loadData()
	uploadData()





