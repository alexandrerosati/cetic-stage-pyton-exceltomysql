#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse

parser = argparse.ArgumentParser(description='Convertisseur latin1 vers utf-8')

parser.add_argument('--source', action="store", dest="source", help='fichier source de donnée')
parser.add_argument('--cible', action="store", dest="cible", help='fichier cible de donnée')

file_in = parser.parse_args().source
file_out = parser.parse_args().cible

BLOCKSIZE = 1024*1024
with open(file_in, "rb") as inf:
	with open(file_out, "wb") as ouf:
		while True:
			data = inf.read(BLOCKSIZE)
			if not data: break
			converted = data.decode('latin1').encode('utf8')
			ouf.write(converted)
